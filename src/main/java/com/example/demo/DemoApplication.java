package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here! and we will see how we can do";
	}

	@GetMapping("/judas")
	String homie() {
		return  "Judas where are you?";
	}
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}